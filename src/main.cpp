#include "SDL2/SDL.h"
#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <list>


#define TICK_INTERVAL 32


using namespace std;

Uint32 EVENT_RENDER;

int main(int argc, char **args) {
  SDL_Event event;
  SDL_Window *display = NULL;
  SDL_Renderer *renderer = NULL;
  int WIDTH = 480;
  int HEIGHT = 800;

  static SDL_Event renderEvent{
    .type = EVENT_RENDER
  };

  SDL_LogSetAllPriority(SDL_LOG_PRIORITY_INFO);

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER) < 0) {
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "SDL_Init failed: %s", SDL_GetError());
    atexit(SDL_Quit);
    exit(1);
  }

  int windowFlags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN;

  display = SDL_CreateWindow("Touch tester", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, WIDTH,
                            HEIGHT, windowFlags);
  if (display == NULL) {
    fprintf(stderr, "ERROR: Could not create window/display: %s\n",
            SDL_GetError());
    atexit(SDL_Quit);
    exit(1);
  }

  renderer = SDL_CreateRenderer(display, -1, 0);

  if (renderer == NULL) {
    SDL_LogError(SDL_LOG_CATEGORY_VIDEO,
                "ERROR: Could not create renderer: %s\n", SDL_GetError());
    atexit(SDL_Quit);
    exit(1);
  }

  if (SDL_SetRenderDrawColor(renderer, 128, 128, 128, SDL_ALPHA_OPAQUE) != 0) {
    SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "ERROR: Could not set background color: %s\n", SDL_GetError());
    atexit(SDL_Quit);
    exit(1);
  }

  if (SDL_RenderFillRect(renderer, NULL) != 0) {
    SDL_LogError(SDL_LOG_CATEGORY_VIDEO,
                "ERROR: Could not fill background color: %s\n",
                SDL_GetError());
    atexit(SDL_Quit);
    exit(1);
  }

  // Start drawing keyboard when main loop starts
  SDL_PushEvent(&renderEvent);

  bool isTouching=false;
  bool isClicking=false;
  unsigned int xTouch, yTouch;

  // The Main Loop.
  while (1) {
    if (SDL_WaitEvent(&event)) {

      // an event was found
      switch (event.type) {
	case SDL_FINGERDOWN:
	  isTouching=true;
          xTouch = event.tfinger.x * WIDTH;
          yTouch = event.tfinger.y * HEIGHT;
          SDL_PushEvent(&renderEvent);
	  break; //SDL_FINGERDOWN
        case SDL_FINGERUP:
	  isTouching=false;
          SDL_PushEvent(&renderEvent);
          break; // SDL_FINGERUP
	case SDL_FINGERMOTION:
          xTouch = event.tfinger.x * WIDTH;
          yTouch = event.tfinger.y * HEIGHT;
          SDL_PushEvent(&renderEvent);
	  break;
	case SDL_MOUSEBUTTONDOWN:
          isClicking=true;
	  xTouch = event.motion.x;
	  yTouch = event.motion.y;
          SDL_PushEvent(&renderEvent);
	  break;
	case SDL_MOUSEBUTTONUP:
          isClicking=false;
          SDL_PushEvent(&renderEvent);
	  break;
	case SDL_MOUSEMOTION:
          xTouch = event.motion.x;
          yTouch = event.motion.y;
          SDL_PushEvent(&renderEvent);
	  break;
      } // switch event.type

      if (event.type == SDL_QUIT) {
         goto QUIT;
      }

      // Render event handler
      if (event.type == EVENT_RENDER){	
	if (isTouching) {
	  SDL_SetRenderDrawColor(renderer, 255,255,255, SDL_ALPHA_OPAQUE);
	} else if (isClicking) {
	  SDL_SetRenderDrawColor(renderer, 0,0,0, SDL_ALPHA_OPAQUE);
	} else {
	  SDL_SetRenderDrawColor(renderer, 128,128,128, SDL_ALPHA_OPAQUE);
	}
	SDL_RenderFillRect(renderer, NULL);
	if (isTouching || isClicking ) {
	  SDL_SetRenderDrawColor(renderer, 128,128,128, SDL_ALPHA_OPAQUE);
	  SDL_RenderDrawLine(renderer, xTouch, 0, xTouch, HEIGHT);
	  SDL_RenderDrawLine(renderer, 0, yTouch, WIDTH, yTouch);
	}
        SDL_RenderPresent(renderer);
      }
    } // event handle loop
  }   // main loop

QUIT:
  SDL_QuitSubSystem(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER);
  atexit(SDL_Quit);
  return 0;
}
